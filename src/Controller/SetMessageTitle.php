<?php

namespace Drupal\synmail\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * SetMessageTitle.
 */
class SetMessageTitle extends ControllerBase {

  /**
   * Set Title.
   */
  public static function set($entity) {
    if (empty($entity->subject->getValue())) {
      $formId = $entity->contact_form->getString();
      $entityForm = \Drupal::entityManager()->getStorage('contact_form')->load($formId);
      $formTitle = !empty($entityForm->get('label')) ? $entityForm->get('label') : 'Контактная форма';
      $entity->subject->setValue($formTitle);
    }
  }

}

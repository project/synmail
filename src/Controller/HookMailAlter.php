<?php

namespace Drupal\synmail\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

/**
 * Defines the default Drupal mail backend, using PHP's native mail() function.
 */
class HookMailAlter extends ControllerBase {

  /**
   * Common Sender.
   */
  public static function hook(&$message) {
    $config = \Drupal::config('synmail.settings');
    if ($message['id'] == 'contact_page_mail') {
      // Свой темплейт.
      if ($config->get('tpl') && isset($message['params']['contact_message'])) {
        $message['body'][0] = Markup::create(self::getWarning());
        $message['body'][1] = Markup::create(self::getMessage($message));
      }
      // Добавить поулчателей.
      self::addEmails($config, $message);
    }

    // От кого слать письма.
    if ($config->get('from')) {
      $message['headers']['From'] = $config->get('from');
    }

    // Добавить в шапку признак хтмл.
    if ($config->get('html')) {
      $message['headers']['Content-Type'] = 'text/html';
    }
    // Использовать свой пхпмейл.
    if ($config->get('phpmail')) {
      $message['send'] = FALSE;
      PhpMail::mail($message);
    }
  }

  /**
   * Добавить поулчателей.
   */
  public static function addEmails($config, &$message) {
    if ($config->get('emails')) {
      $to = [];
      $to[] = $message['to'];
      $emails = $config->get('emails');
      $emails = explode("\n", $emails);
      foreach ($emails as $email) {
        if (strpos($email, "@") && strpos($email, ".")) {
          $to[] = trim($email);
        }
      }
      $message['to'] = implode(', ', $to);
    }
  }

  /**
   * Нормальное сообщение.
   */
  public static function getMessage($message) {
    $submission = $message['params']['contact_message']->toArray();
    $form = $message['params']['contact_form']->toArray();
    $msg = [];
    foreach ($submission as $key => $value) {
      if (strpos($key, 'field_') !== FALSE) {
        $fieldDefinition = $message['params']['contact_message']->$key->getFieldDefinition();
        $label = $fieldDefinition->label();
        $val = '—';
        if (isset($value[0]['value'])) {
          $val = $value[0]['value'];
        }
        $msg["field-$key"] = [
          '#prefix' => "<div>",
          'title' => ['#markup' => "<b>$label:</b> "],
          'data' => ['#markup' => $val],
          '#suffix' => "</div>\n",
        ];
      }
    }
    return \Drupal::service('renderer')->render($msg);
  }

  /**
   * Предостережение.
   */
  public static function getWarning() {
    $warning['#markup'] = "
<div>
<h2>Вам письмо от сайта</h2>
<ul>
  <li>Заявка пришла с технического адреса, не стоит нажимать 'ответить' и отправлять ответ нам.
E-mail клиента (если он его оставил) находится где-то в письме.</li>
  <li>До того как начать писать e-mail посмотри - может клиент оставил телефон,
в таком случае лучше прямо сейчас ему позвонить, сообщить что заявка получена, и передана в работу.</li>
<ul>
</div>
<hr>
<h2>Содержимое заявки</h2>
";
    $html = \Drupal::service('renderer')->render($warning);
    return $html;
  }

}
